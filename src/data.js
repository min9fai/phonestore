export const phonesData = [
  {
    id: '1',
    name: 'iPhone X',
    image: 'https://facebook.github.io/react-native/docs/assets/favicon.png',
    price: 1000,
    discount: .5,
    inventory: 0,
    detail:
      `AntDesign by AntFinance (297 icons)
Entypo by Daniel Bruce (411 icons)
EvilIcons by Alexander Madyankin & Roman Shamin (v1.10.1, 70 icons)
Feather by Cole Bemis & Contributors (v4.7.0, 266 icons)
FontAwesome by Dave Gandy (v4.7.0, 675 icons)
FontAwesome 5 by Fonticons, Inc. (v5.5.0, 1409 (free) 4566 (pro) icons)
Foundation by ZURB, Inc. (v3.0, 283 icons)
Ionicons by Ben Sperry (v4.2.4, 696 icons)
MaterialIcons by Google, Inc. (v3.0.1, 932 icons)
MaterialCommunityIcons by MaterialDesignIcons.com (v2.8.94, 2894 icons)
Octicons by Github, Inc. (v8.0.0, 177 icons)
Zocial by Sam Collins (v1.0, 100 icons)
SimpleLineIcons by Sabbir & Contributors (v2.4.1, 189 icons)`
  }, {
    id: '2',
    name: 'iPhone XR',
    image: 'https://facebook.github.io/react-native/docs/assets/favicon.png',
    price: 1200,
    discount: .4,
    inventory: 4,
    detail:
      `AntDesign by AntFinance (297 icons)
Entypo by Daniel Bruce (411 icons)
EvilIcons by Alexander Madyankin & Roman Shamin (v1.10.1, 70 icons)
Feather by Cole Bemis & Contributors (v4.7.0, 266 icons)
FontAwesome by Dave Gandy (v4.7.0, 675 icons)
FontAwesome 5 by Fonticons, Inc. (v5.5.0, 1409 (free) 4566 (pro) icons)
Foundation by ZURB, Inc. (v3.0, 283 icons)
Ionicons by Ben Sperry (v4.2.4, 696 icons)
MaterialIcons by Google, Inc. (v3.0.1, 932 icons)
MaterialCommunityIcons by MaterialDesignIcons.com (v2.8.94, 2894 icons)
Octicons by Github, Inc. (v8.0.0, 177 icons)
Zocial by Sam Collins (v1.0, 100 icons)
SimpleLineIcons by Sabbir & Contributors (v2.4.1, 189 icons)`
  }, {
    id: '3',
    name: 'iPhone XRR',
    image: 'https://facebook.github.io/react-native/docs/assets/favicon.png',
    price: 1300,
    discount: 1,
    inventory: 10,
    detail:
      `AntDesign by AntFinance (297 icons)
Entypo by Daniel Bruce (411 icons)
EvilIcons by Alexander Madyankin & Roman Shamin (v1.10.1, 70 icons)
Feather by Cole Bemis & Contributors (v4.7.0, 266 icons)
FontAwesome by Dave Gandy (v4.7.0, 675 icons)
FontAwesome 5 by Fonticons, Inc. (v5.5.0, 1409 (free) 4566 (pro) icons)
Foundation by ZURB, Inc. (v3.0, 283 icons)
Ionicons by Ben Sperry (v4.2.4, 696 icons)
MaterialIcons by Google, Inc. (v3.0.1, 932 icons)
MaterialCommunityIcons by MaterialDesignIcons.com (v2.8.94, 2894 icons)
Octicons by Github, Inc. (v8.0.0, 177 icons)
Zocial by Sam Collins (v1.0, 100 icons)
SimpleLineIcons by Sabbir & Contributors (v2.4.1, 189 icons)`
  }
]

export const userData = [
  {
    id: 1,
    account: 'test1',
    password: '123456',
    name: 'Peter 1',
    avaster: '',
    email: 'mail1@mail.com',
    address: 'address 1',
  },
  {
    id: 2,
    account: 'test2',
    password: '123456',
    name: 'Peter 2',
    avaster: '',
    email: 'mail2@mail.com',
    address: 'address 2',
  },
  {
    id: 3,
    account: 'test3',
    password: '123456',
    name: 'Peter 3',
    avaster: '',
    email: 'mail3@mail.com',
    address: 'address 3',
  }
]