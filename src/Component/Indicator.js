import React from 'react'
import {
	View,
	StyleSheet,
	ActivityIndicator,
} from 'react-native'

import { size, color } from '../Common'

const Indicator = ({ loading }) => {
	if (!loading)
		return null
	
	return (
		<View style={styles.container}>
			<ActivityIndicator
				animating={loading}
				size='large'
				style={styles.activityIndicator}
				color={color.main}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: color.transparent,
		position: 'absolute',
		height: '100%',
		width: '100%',
	},
	activityIndicator: {
		left: (size.width - 80) / 2,
		position: 'absolute',
		height: 80,
		width: 80,
		bottom: 90,
	},
})

export default Indicator