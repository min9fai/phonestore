import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { color, size } from '../../Common'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import IconIon from 'react-native-vector-icons/Ionicons'
import IconEntypo from 'react-native-vector-icons/Entypo'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Feather from 'react-native-vector-icons/Feather'

const IconButton = ({ iconName, onPress = () => { },
	iconColor = color.white, cusStyle = {}, iconType = 1 }) => {
	let iconRender
	switch (iconType) {
		case 2:
			iconRender = <IconIon name={iconName} size={size.fs_large} color={iconColor} />
			break;
		case 3:
			iconRender = <IconEntypo name={iconName} size={size.fs_large} color={iconColor} />
			break;
		case 4:
			iconRender = <FontAwesome name={iconName} size={size.fs_large} color={iconColor} />
			break;
		case 5:
			iconRender = <AntDesign name={iconName} size={size.fs_large} color={iconColor} />
			break;
		case 6:
			iconRender = <Feather name={iconName} size={size.fs_large} color={iconColor} />
			break;
		default:
			iconRender = <FontAwesome5 name={iconName} size={size.fs_large} color={iconColor} />
			break;
	}

	return (
		<TouchableOpacity onPress={onPress}>
			<View style={[styles.container, cusStyle]}>
				{iconRender}
			</View>
		</TouchableOpacity>
	)
}

const styles = StyleSheet.create({
	container: {
		width: size.headerButtonSize,
		height: size.headerButtonSize,
		justifyContent: 'center',
		alignItems: 'center',
	}
})

export default IconButton

