import React from 'react'
import { View, StyleSheet, TouchableWithoutFeedback, Text } from 'react-native'
import { color, size } from '../../Common'


const CusButton = ({ title, onPress = () => { }, disabled,
  cusContainerStyle = {}, cusWrapContainerStyle = {}, cusTitleStyle = {} }) => {

  const disableStyle = disabled ? { backgroundColor: color.btn_disable }
    : {}
  return (
    <TouchableWithoutFeedback onPress={onPress} >
      <View style={[styles.container, cusContainerStyle, disableStyle]}>
        <View style={[styles.wrapContainer, cusWrapContainerStyle]}>
          <Text style={[styles.title, cusTitleStyle]}>{title}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 40,
    minWidth: 100,
    marginLeft: 10,
    marginRight: 10,
    padding: 5,
    backgroundColor: color.white,
  },
  wrapContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  title: {

  }
})

export default CusButton

