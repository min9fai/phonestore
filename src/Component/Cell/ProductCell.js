import React from 'react'
import { StyleSheet, View, Image, Text, TouchableWithoutFeedback } from 'react-native'
import { size, color } from '../../Common'
import IconButton from '../Button/IconButton'
import ProductNameView from '../Product/ProductNameView'
import ProductPriceView from '../Product/ProductPriceView'

const ProductCell = ({ item, index, navigation }) => {
  return (
    <TouchableWithoutFeedback onPress={() => {
      const { navigate } = navigation
      navigate('ProductDetailPage', { selectedProduct: item })
    }}>
      <View style={styles.container}>
        <View style={styles.wrapContainer}>
          <Image
            style={styles.image}
            source={{ uri: item.image }}
          />
          <ProductNameView product={item} cusContainerStyle={styles.itemView} />
          <ProductPriceView product={item} cusContainerStyle={styles.itemView} />
          <View style={{ flex: 1 }}></View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  container: {
    height: size.width / 2,
    width: size.width / 2,
  },
  wrapContainer: {
    margin: 5,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: size.width / 2 - 80,
    width: size.width / 2 - 80,
  },
  itemView: {
    height: 30,
    justifyContent: 'center',
  }
})

export default ProductCell