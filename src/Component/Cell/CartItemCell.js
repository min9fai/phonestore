import React from 'react'
import { StyleSheet, View, Image, Text, TouchableWithoutFeedback } from 'react-native'
import { connect } from 'react-redux'
import { doAddCartAction, doReduceCartAction } from '../../Redux/Action/Action'
import { size, color, tools } from '../../Common'
import IconButton from '../Button/IconButton'
import ProductNameView from '../Product/ProductNameView'
import ProductPriceView from '../Product/ProductPriceView'

class CartItemCell extends React.PureComponent {
  constructor(props) {
    super(props)

    this.addCartTap = this.addCartTap.bind(this)
    this.reduceCartTap = this.reduceCartTap.bind(this)
    this.removeCartTap = this.removeCartTap.bind(this)
  }

  addCartTap() {
    const { item, index, navigation, doAddCartAction } = this.props
    if (item.amount >= 2) {
      tools.showAlert('每樣物品不可購買多於2件!')
    } else {
      doAddCartAction(item)
    }
  }

  reduceCartTap() {
    const { item, index, navigation, doReduceCartAction } = this.props
    doReduceCartAction(item)
  }

  removeCartTap() {
    const { item, index, navigation, doReduceCartAction } = this.props
    doReduceCartAction(item, 0)
  }

  render() {
    const { item, index, navigation } = this.props
    return (
      <TouchableWithoutFeedback onPress={() => {
        const { navigate } = navigation
      }}>
        <View style={[styles.container, {}]}>
          <View style={styles.wrapContainer}>
            <View style={styles.topContainer}>
              <Text style={{ flex: 1 }}>訂單{index + 1}</Text>
              <IconButton iconName={'close'} iconType={5}
                cusStyle={{ height: 25, width: 25 }} iconColor={color.black}
                onPress={this.removeCartTap} />
            </View>
            <View style={styles.midContainer}>
              <Image
                style={styles.image}
                source={{ uri: item.image }}
              />
              <View style={styles.contentContainer}>
                <View style={styles.nameContainer}>
                  <Text style={{ flex: 1, overflow: 'hidden' }} >{item.name}</Text>
                  <Text>${item.price * item.discount}</Text>
                </View>
                <View style={{ flex: 1 }} />
                <View style={styles.amountContainer}>
                  <IconButton iconName={'minuscircleo'} iconType={5}
                    iconColor={color.main} cusStyle={{ height: 30, width: 30 }}
                    onPress={this.reduceCartTap} />
                  <Text>{item.amount}</Text>
                  <IconButton iconName={'pluscircleo'} iconType={5}
                    iconColor={color.main} cusStyle={{ height: 30, width: 30 }}
                    onPress={this.addCartTap} />
                  <View style={{ flex: 1 }} />
                  <Text>總值: ${item.price * item.discount * item.amount}</Text>
                </View>
              </View>
            </View>
            <View style={styles.botContainer}>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    height: 130,
    width: size.width,
  },
  wrapContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  topContainer: {
    paddingLeft: 5,
    paddingRight: 5,
    height: 30,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.divier,
  },
  midContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  botContainer: {
    height: 0,
    width: '100%',
    backgroundColor: color.divier,
  },
  image: {
    height: 90,
    width: 90,
    marginRight: 5,
  },
  contentContainer: {
    padding: 5,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  nameContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  amountContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }

})

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doAddCartAction: (product) => dispatch(doAddCartAction(product)),
    doReduceCartAction: (product, amount) => dispatch(doReduceCartAction(product, amount)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartItemCell)