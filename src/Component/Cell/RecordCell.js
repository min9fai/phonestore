import React from 'react'
import { StyleSheet, View, Image, Text, TouchableWithoutFeedback } from 'react-native'
import { size, color } from '../../Common'
import IconButton from '../Button/IconButton'
import ProductNameView from '../Product/ProductNameView'
import ProductPriceView from '../Product/ProductPriceView'

let count = 0
const RecordCell = ({ item, index, navigation }) => {
    const productsRender = item.products.map(product => {
        count++
        return (
            <View style={styles.itemView} key={'view_' + count}>
                <Text style={{ flex: 1 }} >{product.name}</Text>
                <Text >${product.price * product.discount}x{product.stock}</Text>
            </View>
        )
    })
    return (
        <TouchableWithoutFeedback onPress={() => {

        }}>
            <View style={styles.container}>
                <View style={styles.wrapContainer}>
                    <View style={[styles.itemView, { backgroundColor: color.main }]}>
                        <Text>{'單號：'}</Text>
                        <Text style={{ flex: 1 }}>{item.ordernumber}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: color.divier, marginBottom: 5, }} />
                    <View style={[styles.itemView, {
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                    }]}>
                        <Text style={{ marginBottom: 5 }}>{'內容：'}</Text>
                        {productsRender}
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: color.divier, marginBottom: 5, }} />


                    <View style={styles.itemView}>
                        <Text>{'總價：'}</Text>
                        <Text style={{ flex: 1 }}>${item.totalprice}</Text>
                    </View>
                    <View style={styles.itemView}>
                        <Text>{'送貨地址：'}</Text>
                        <Text style={{ flex: 1 }}>{item.address}</Text>
                    </View>
                    <View style={styles.itemView}>
                        <Text>{'訂單狀態：'}</Text>
                        <Text style={{ flex: 1 }}>{item.status}</Text>
                    </View>
                    <View style={styles.itemView}>
                        <Text>{'建立日期：'}</Text>
                        <Text style={{ flex: 1 }}>{item.createdAt}</Text>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: color.divier, marginBottom: 5, }} />
                </View>
            </View>
        </TouchableWithoutFeedback >
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: 150,
        width: size.width,
    },
    wrapContainer: {
        margin: 5,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemView: {
        padding: 5,
        minHeight: 20,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default RecordCell