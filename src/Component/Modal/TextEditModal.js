/*
@flow
*/

import React from 'react'
import { KeyboardAvoidingView, StyleSheet, Modal, View, Text, TextInput, TouchableWithoutFeedback } from 'react-native'
import { color } from '../../Common';
import IconButton from '../Button/IconButton';

type Props = {
  isShowModal: boolean,
  closeModal: any,

  placeHolder: ?string,
  defValue: ?string,
  title: ?string,
}

type States = {
  textValue: string,
}

class TextEditModal extends React.PureComponent<Props, States> {
  constructor(props) {
    super(props)

    this.state = {
      textValue: props.defValue
    }

    this.inputRef = undefined
    this.onCloseModal = this.onCloseModal.bind(this)
    this.onShow = this.onShow.bind(this)
    this.onChangeText = this.onChangeText.bind(this)

    this.cancelTap = this.cancelTap.bind(this)
    this.confirmTap = this.confirmTap.bind(this)
  }

  onCloseModal() {
    if (this.state.textValue && this.state.textValue.trim() !== '')
      this.props.closeModal()
  }

  onChangeText(text) {
    this.setState({
      textValue: text,
    })
  }

  onShow() {
    this.setState({
      textValue: this.props.defValue,
    })

    if (this.inputRef) {
      this.inputRef.focus()
    }
  }

  cancelTap() {
    this.props.closeModal()
  }

  confirmTap() {
    if (this.state.textValue && this.state.textValue.trim() !== '')
      this.props.closeModal(this.state.textValue)
  }

  render() {
    return (
      <Modal visible={this.props.isShowModal}
        transparent={true}
        onDismiss={() => { }}
        onShow={this.onShow}>
        <TouchableWithoutFeedback onPress={this.onCloseModal}>
          <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <TouchableWithoutFeedback onPress={null}>
              <View style={styles.wrapContainer}>
                <Text style={styles.title}>{this.props.title}</Text>
                <TextInput
                  ref={ref => this.inputRef = ref}
                  value={this.state.textValue}
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  style={styles.input} placeholder={this.props.placeHolder}
                  clearButtonMode={'while-editing'}
                  onChangeText={this.onChangeText}
                />
                <View style={styles.buttonView}>
                  <IconButton iconName={'close'} iconType={5}
                    cusStyle={{ height: 30, width: 30, marginRight: 5, }} iconColor={'#f00'} onPress={this.cancelTap} />
                  <IconButton iconName={'check'} iconType={5}
                    cusStyle={{ height: 30, width: 30, marginLeft: 5, }} iconColor={'#0f0'}
                    onPress={this.confirmTap} />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000095'
  },
  wrapContainer: {
    backgroundColor: color.white,
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  title: {
    margin: 10,
  },
  input: {
    paddingRight: 5,
    paddingLeft: 5,
    height: 30,
    width: 250,
    borderColor: 'gray',
    borderWidth: 1,
  },
  buttonView: {
    marginTop: 5,
    height: 30,
    width: 250,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  }
})

export default TextEditModal