import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { size, color } from '../../Common'

const ProductNameView = ({ product, cusContainerStyle = {} }) => {

  const inventoryStr = product.stock > 0 ? '' : '缺貨'
  const inventoryColor = product.stock > 0 ? color.has_inventory : color.no_inventory

  return (
    <View style={[styles.container, cusContainerStyle]} >
      <Text style={styles.title}>{product.name}</Text>
      <Text style={[styles.stock, { color: inventoryColor }]}>{inventoryStr}</Text>
    </View >
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  title: {
    margin: 5,
    fontSize: size.fs_normal,
  },
  inventory: {
    marginTop: 5,
    fontSize: size.fs_smaller,
  }
})

export default ProductNameView