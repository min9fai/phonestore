import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { size, color } from '../../Common'

const ProductPriceView = ({ product, cusContainerStyle = {} }) => {

  const price = product.price * product.discount
  const cost = product.discount == 1 ? '' : '$' + product.price
  const discountStyle = cost ? { fontWeight: 'bold', color: color.has_discount } : { color: color.black }

  return (
    <View style={[styles.container, cusContainerStyle]}>
      <Text style={[styles.title, discountStyle]}>${price}</Text>
      <Text style={styles.discount}>{cost}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  title: {
    margin: 5,
    fontSize: size.fs_normal,
  },
  discount: {
    marginTop: 5,
    fontSize: size.fs_small,
    textDecorationLine: 'line-through'
  }
})

export default ProductPriceView