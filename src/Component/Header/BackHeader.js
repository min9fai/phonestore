import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { connect } from 'react-redux'
import { size, color } from '../../Common'
import IconButton from '../Button/IconButton'

const BackHeader = ({ userInfo, navigation, showLogin, title }) => {
  const loginStatusIcon = userInfo ? 'user' : 'login'

  return (
    <View style={styles.container}>
      <View style={styles.wrapContainer}>
        <IconButton iconName='chevron-left' cusStyle={styles.button}
          onPress={
            () => { navigation.goBack() }
          }
        />
        <View style={{ flex: 1 }} />
        <Text style={styles.title}>{title}</Text>
        <View style={{ flex: 1 }} />
        {
          showLogin &&
          <IconButton iconName={loginStatusIcon} iconType={5} cusStyle={styles.button}
            onPress={
              () => {
                if (!userInfo) {
                  navigation.navigate('LoginPage')
                } else {
                  navigation.navigate('ProfilePage')
                }
              }
            }
          />
        }
        {
          !showLogin && <View style={styles.button} />
        }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: size.headerHeight,
    width: '100%',
    backgroundColor: color.main,
  },
  wrapContainer: {
    flex: 1,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: size.headerButtonSize,
    height: size.headerButtonSize,
  },
  title: {
    color: color.white,
    fontSize: size.fs_bigger,
    fontWeight: 'bold',
  }
})

const mapStateToProps = state => {
  return {
    userInfo: state.userReducer.userInfo,

    cartList: state.productReducer.cartList,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BackHeader)