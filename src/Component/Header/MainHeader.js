import React from 'react'
import { StyleSheet, View, Text, Image, TouchableWithoutFeedback } from 'react-native'
import { connect } from 'react-redux'
import { size, color } from '../../Common'
import IconButton from '../Button/IconButton'

const MainHeader = (props) => {
  const { hasBack, showIcon } = props
  const loginStatusIcon = props.userInfo ? 'user' : 'login'
  let totalItem = 0
  props.cartList.forEach(item => {
    totalItem += item.amount
  })
  const badge = totalItem > 99 ? '99+' : totalItem

  return (
    <View style={styles.container}>
      <View style={styles.wrapContainer}>
        {
          showIcon &&
          <Image style={styles.appIcon}
            source={require('../../Images/smartphone.png')}
          />
        }
        {hasBack &&
          <IconButton iconName='chevron-left' cusStyle={styles.button}
            onPress={
              () => { props.navigation.goBack() }
            }
          />
        }
        <View style={{ flex: 1 }} />
        <TouchableWithoutFeedback onPress={
          () => { props.navigation.navigate('CartPage') }
        }>
          <View style={styles.button}>
            <IconButton iconName='shopping-cart' iconType={6} cusStyle={styles.button} onPress={
              () => { props.navigation.navigate('CartPage') }
            } />
            {
              badge !== 0 &&
              <View style={styles.badge}>
                <Text style={styles.badgeText}>{badge}</Text>
              </View>
            }
          </View>
        </TouchableWithoutFeedback>
        <IconButton iconName={loginStatusIcon} iconType={5} cusStyle={styles.button}
          onPress={
            () => {
              if (!props.userInfo) {
                props.navigation.navigate('LoginPage')
              } else {
                props.navigation.navigate('ProfilePage')
              }
            }
          }
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: size.headerHeight,
    width: '100%',
    backgroundColor: color.main,
  },
  wrapContainer: {
    flex: 1,
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  appIcon: {
    padding: 5,
    width: 30,
    height: 30,
  },
  badge: {
    position: 'absolute',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    right: 0,
    width: 20,
    height: 20,
    borderRadius: 20 / 2,
    backgroundColor: color.badge,
  },
  badgeText: {
    fontSize: size.fs_small,
    color: color.white,
  },
  button: {
    width: size.headerButtonSize,
    height: size.headerButtonSize,
  }
})

const mapStateToProps = state => {
  return {
    userInfo: state.userReducer.userInfo,

    cartList: state.productReducer.cartList,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainHeader)
