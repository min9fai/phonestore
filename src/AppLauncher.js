import React from 'react'
import { StyleSheet, View } from 'react-native'
import Routes from './Routes'

class AppLauncher extends React.PureComponent {
  constructor(props) {
    super(props)
  }

  render() {

    return (
      <Routes />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

export default AppLauncher