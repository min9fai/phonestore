import ActionType from '../Action/ActionType'
import I18n from '../../Language'

const initState = {
	activityKey: '',
	locale: I18n.locale,
}

export const baseReducer = (state = initState, action) => {
	return state
}

