import { baseReducer } from './BaseReducer'
import { productReducer } from './ProductReducer'
import { userReducer } from './UserReducer'
import { reducer as network } from 'react-native-offline'

export {
	userReducer,
	productReducer,
	baseReducer,
  network,
}
