import ActionType from '../Action/ActionType'
import { config } from '../../Common'

const initState = {
	loginLoading: false,
	logoutLoading: false,
	registerLoading: false,
	updateUserLoading: false,
	fetchRecordLoading: false,

	userInfo: undefined,
	myReocrd: [],
}

export const userReducer = (state = initState, action) => {
	switch (action.type) {
		case ActionType.LOGIN_START:
			return { ...state, loginLoading: true }
		case ActionType.LOGIN_SUCCESS:
			return { ...state, loginLoading: false, userInfo: action.body.userInfo }
		case ActionType.LOGIN_FAIL:
			return { ...state, loginLoading: false, userInfo: undefined }

		case ActionType.LOGOUT_START:
			return { ...state, logoutLoading: true }
		case ActionType.LOGOUT_SUCCESS:
			return { ...state, logoutLoading: false, userInfo: undefined }
		case ActionType.LOGOUT_FAIL:
			return { ...state, logoutLoading: false, userInfo: undefined }

		case ActionType.REGISTER_START:
			return { ...state, registerLoading: true }
		case ActionType.REGISTER_SUCCESS:
		case ActionType.REGISTER_FAIL:
			return { ...state, registerLoading: false }

		case ActionType.UPDATE_USER_START:
			return { ...state, updateUserLoading: true }
		case ActionType.UPDATE_USER_SUCCESS: {
			const newAddress = action.body.address
			return {
				...state, updateUserLoading: false,
				userInfo: { ...state.userInfo, address: newAddress }
			}
		}
		case ActionType.UPDATE_USER_FAIL:
			return { ...state, updateUserLoading: false }

		case ActionType.FETCH_RECORD_START:
			return { ...state, fetchRecordLoading: true }
		case ActionType.FETCH_RECORD_SUCCESS: {
			const records = action.body.records
			return { ...state, fetchRecordLoading: false, myReocrd: records }
		}
		case ActionType.FETCH_RECORD_FAIL:
			return { ...state, fetchRecordLoading: false, myReocrd: [] }
		default:
			return state
	}
}

