import ActionType from '../Action/ActionType'
import { config } from '../../Common'

const initState = {
  fetchDataLoading: false,
  addCartLoading: false,
  reduceCartLoading: false,
  paymentLoading: false,

  productList: [],
  cartList: [],
}

export const productReducer = (state = initState, action) => {
  switch (action.type) {
    //fetch data
    case ActionType.FETCH_DATA_START:
      return { ...state, fetchDataLoading: true }
    case ActionType.FETCH_DATA_SUCCESS:
      {
        const { products } = action.body
        return { ...state, fetchDataLoading: false, productList: products }
      }
    case ActionType.FETCH_DATA_FAIL:
      return { ...state, fetchDataLoading: false }

    //add cart
    case ActionType.ADD_CART_START:
      return { ...state, addCartLoading: true }
    case ActionType.ADD_CART_SUCCESS:
      {
        const { product } = action.body
        let exist = false
        const newCartList = state.cartList.map(item => {
          if (item.id === product.id) {
            exist = true
            item.amount++
          }
          return item
        })

        if (!exist)
          newCartList.push({ ...product, amount: 1 })
        return { ...state, addCartLoading: false, cartList: newCartList }
      }
    case ActionType.ADD_CART_FAIL:
      return { ...state, addCartLoading: false }

    //reduce cart
    case ActionType.REDUCE_CART_START:
      return { ...state, reduceCartLoading: true }
    case ActionType.REDUCE_CART_SUCCESS:
      {
        const { product, amount } = action.body
        let newCartList = []
        if (amount <= 0) {
          newCartList = state.cartList.filter(item => {
            return item.id !== product.id
          })
        } else {
          let needRemove = false
          newCartList = state.cartList.map(item => {
            if (item.id === product.id) {
              item.amount = item.amount - amount
              needRemove = item.amount <= 0
            }
            return item
          })

          if (needRemove) {
            newCartList = state.cartList.filter(item => {
              return item.id !== product.id
            })
          }
        }
        return { ...state, reduceCartLoading: false, cartList: newCartList }
      }
    case ActionType.REDUCE_CART_FAIL:
      return { ...state, reduceCartLoading: false }

    //payment
    case ActionType.PAYMENT_START:
      return { ...state, paymentLoading: true }
    case ActionType.PAYMENT_SUCCESS: {
      const productList = action.body.productList
      return { ...state, paymentLoading: false, cartList: [], productList: productList }
    }
    case ActionType.PAYMENT_FAIL:
      return { ...state, paymentLoading: false }
    default:
      return state
  }
}

