import { AsyncStorage } from 'react-native'
import ActionType from './ActionType'
import { config, api, constant } from '../../Common'
import * as data from '../../data'

export const createAction = (type, body = {}) => {
	return {
		type: type,
		body: body
	}
}

export const doLoginAction = (account, password) => {
	return async (dispatch, getState) => {
		dispatch(createAction(ActionType.LOGIN_START))

		const result = await api.login(account, password)

		if (result && result.result) {
			dispatch(createAction(ActionType.LOGIN_SUCCESS, { userInfo: result.user }))
		} else {
			dispatch(createAction(ActionType.LOGIN_FAIL))
		}

		return result
	}
}

export const doLogoutAction = () => {
	return async (dispatch, getState) => {
		const { userReducer } = getState()

		dispatch(createAction(ActionType.LOGOUT_START))

		const result = await api.logout(userReducer.userInfo.token)

		if (result && result.result) {
			dispatch(createAction(ActionType.LOGOUT_SUCCESS))
		} else {
			dispatch(createAction(ActionType.LOGOUT_FAIL))
		}

		return result
	}
}

export const doRegisterAction = (last_name, first_name, account, password, email, address) => {
	return async (dispatch, getState) => {
		dispatch(createAction(ActionType.REGISTER_START))

		const result = await api.register(last_name, first_name, account, password, email, address)

		if (result && result.result) {
			dispatch(createAction(ActionType.REGISTER_SUCCESS))
		} else {
			dispatch(createAction(ActionType.REGISTER_FAIL))
		}

		return result
	}
}

export const doUpdateUserAction = (address) => {
	return async (dispatch, getState) => {
		const { userReducer } = getState()

		dispatch(createAction(ActionType.UPDATE_USER_START))

		const result = await api.updateUserAddress(userReducer.userInfo.token, address)
		if (result && result.result) {
			dispatch(createAction(ActionType.UPDATE_USER_SUCCESS, { address: address }))
		} else {
			dispatch(createAction(ActionType.UPDATE_USER_FAIL))
		}

		return result
	}
}

export const doFetchDataAction = () => {
	return async (dispatch, getState) => {
		dispatch(createAction(ActionType.FETCH_DATA_START))

		const result = await api.fetchProduct()
		if (result && result.result) {
			dispatch(createAction(ActionType.FETCH_DATA_SUCCESS, { products: result.products }))
		} else {
			dispatch(createAction(ActionType.FETCH_DATA_FAIL))
		}

		return result
	}
}

export const doAddCartAction = (product) => {
	return async (dispatch, getState) => {
		const state = getState()
		const productReducer = state.productReducer
		const cartList = productReducer.cartList

		let currentAmount = 0
		for (let i = 0; i < cartList.length; i++) {
			const item = cartList[i]
			if (item.id === product.id) {
				currentAmount = item.amount ? item.amount : 0
				break
			}
		}

		if (currentAmount >= 2) {
			return 'OVER'
		}

		dispatch(createAction(ActionType.ADD_CART_START))
		const result = product

		if (product) {
			dispatch(createAction(ActionType.ADD_CART_SUCCESS, { product }))
		} else {
			dispatch(createAction(ActionType.ADD_CART_FAIL))
		}

		return result
	}
}

export const doReduceCartAction = (product, amount = 1) => {
	return async (dispatch, getState) => {
		dispatch(createAction(ActionType.REDUCE_CART_START))

		const result = product

		if (product) {
			dispatch(createAction(ActionType.REDUCE_CART_SUCCESS, { product, amount }))
		} else {
			dispatch(createAction(ActionType.REDUCE_CART_FAIL))
		}

		return result
	}
}

export const doPaymentAction = (nonce, ordernumber, paymentMethod) => {
	return async (dispatch, getState) => {
		const state = getState()
		const productReducer = state.productReducer
		const userReducer = state.userReducer
		const cartList = productReducer.cartList
		const productList = productReducer.productList

		let totalPrice = 0
		let productlist = ''
		let productamountlist = ''
		const email = userReducer.userInfo.email
		const address = userReducer.userInfo.address
		const token = userReducer.userInfo.token

		cartList.forEach(item => {
			productlist += item.id + ','
			productamountlist += item.amount + ','
			totalPrice += item.price * item.discount * item.amount
		})

		dispatch(createAction(ActionType.PAYMENT_START))
		console.log('doPaymentAction token : ', token)

		const result = await api.createRecord(token, nonce, ordernumber, productlist, totalPrice, email, address, productamountlist, paymentMethod)
		console.log('doPaymentAction result : ', result)
		if (result && result.result) {
			//update product
			const newProductList = productList.map(
				pro => {
					for (let i = 0; i < cartList.length; i++) {
						if (cartList[i].id == pro.id) {
							pro.stock = pro.stock - cartList[i].amount
							break
						}
						return pro
					}
				}
			)

			dispatch(createAction(ActionType.PAYMENT_SUCCESS, { productList: newProductList }))
		} else {
			dispatch(createAction(ActionType.PAYMENT_FAIL))
		}

		return result
	}
}

export const doFetchRecordAction = () => {
	return async (dispatch, getState) => {
		const { userReducer } = getState()

		dispatch(createAction(ActionType.FETCH_RECORD_START))

		const result = await api.fetchRecord(userReducer.userInfo.token)
		if (result && result.result) {
			dispatch(createAction(ActionType.FETCH_RECORD_SUCCESS, { records: result.records }))
		} else {
			dispatch(createAction(ActionType.FETCH_RECORD_FAIL))
		}

		return result
	}
}