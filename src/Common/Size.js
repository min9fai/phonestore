import { Dimensions, Platform, PixelRatio } from 'react-native'
import DeviceInfo from 'react-native-device-info'
import Config from './Config'

export default {
	width: Dimensions.get('window').width,
	height: Dimensions.get('window').height,

	onePixel: 1 / PixelRatio.get(),

	statusBarHeight: Config.isIphoneLH ? 44 :
		Platform.select({ ios: 20, android: 0 }),
	bottomHeight: Config.isIphoneLH ? 34 :
		Platform.select({ ios: 0, android: 0 }),

	headerHeight: 50,
	headerButtonSize: 40,
	tncHeight: 30,
	filterHeight: 50,
	dividerHeight: 1,

	//font
	fs_mini: 8 * PixelRatio.getFontScale(),
	fs_smaller: 10 * PixelRatio.getFontScale(),
	fs_small: 12 * PixelRatio.getFontScale(),
	fs_normal: 14 * PixelRatio.getFontScale(),
	fs_big: 16 * PixelRatio.getFontScale(),
	fs_bigger: 18 * PixelRatio.getFontScale(),
	fs_large: 20 * PixelRatio.getFontScale(),
}