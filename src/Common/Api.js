import { config } from ".";

export async function timeoutPromise(promise) {
  return new Promise((resolve, reject) => {
    const timeoutId = setTimeout(() => {
      resolve(false)
    }, config.connection_timeout)

    promise.then((respone) => {
      clearTimeout(timeoutId)
      if (respone.status === 200) {
        resolve(respone.json())
      } else if (respone.status === 204) {
        resolve(true)
      } else {
        resolve(false)
      }
    }).catch((error) => {
      clearTimeout(timeoutId);
      resolve(false)
    })
  })
}

export const register = async (last_name, first_name, account, password, email, address) => {
  const url = config.api_domain + 'shop/create_user.jsp?last_name=' + last_name +
    '&first_name=' + first_name +
    '&account=' + account +
    '&password=' + password +
    '&email=' + email +
    '&address=' + address;

  const result = await timeoutPromise(
    fetch(
      url, {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        }
      }
    )
  )

  return result
}

export const login = async (account, password) => {
  const url = config.api_domain + 'shop/login_user.jsp?account=' + account + "&password=" + password

  const result = await timeoutPromise(
    fetch(
      url, {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        },
      }
    )
  )

  return result
}

export const logout = async (token) => {
  const url = config.api_domain + 'shop/logout_user.jsp?token=' + token;

  const result = await timeoutPromise(
    fetch(
      url, {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        },
      }
    )
  )

  return result
}

export const updateUserAddress = async (token, address) => {
  const url = config.api_domain + 'shop/update_user.jsp?token=' + token + '&address=' + address

  const result = await timeoutPromise(
    fetch(
      url, {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        },
      }
    )
  )

  return result
}

export const fetchProduct = async () => {
  const url = config.api_domain + 'shop/fetch_product.jsp'

  const result = await timeoutPromise(
    fetch(
      url, {
        method: 'get',
        headers: {
          'content-type': 'application/json'
        },
      }
    )
  )

  return result
}

export const fetchRecord = async (token) => {
  const url = config.api_domain + 'shop/fetch_record.jsp?token=' + token

  const result = await timeoutPromise(
    fetch(
      url, {
        method: 'get',
        headers: {
          'content-type': 'application/json'
        },
      }
    )
  )

  return result
}

export const createRecord = async (token, nonce, ordernumber, productlist, totalprice, email, address, productamountlist, paymentMethod) => {
  //lhp.n911.org/shop/create_record.jsp?token=token_2853&ordernumber=123&productlist=1,2,3&totalprice=5000&email=500@t00.com&address=address&productamountlist=2,2,2
  const url = config.api_domain + 'shop/create_record.jsp?token=' + token +
    '&ordernumber=' + ordernumber +
    '&productlist=' + productlist +
    '&totalprice=' + totalprice +
    '&email=' + email +
    '&address=' + address +
    '&productamountlist=' + productamountlist +
    '&nonce=' + nonce +
    '&paymentMethod=' + paymentMethod


  console.log('createRecord', url)
  const result = await timeoutPromise(
    fetch(
      url, {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        },
      }
    )
  )

  return result
}