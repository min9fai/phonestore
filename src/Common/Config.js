import { Platform, Dimensions } from 'react-native'
import DeviceInfo from 'react-native-device-info'

export default {
  //basic
  app_version: DeviceInfo.getVersion(),
  isIphone: Platform.OS === 'ios',
  isIphoneLH: Platform.OS === 'ios' && Dimensions.get('window').height >= 812,
  uuid: DeviceInfo.getUniqueID(),
  model: DeviceInfo.getModel(),
  device_version: DeviceInfo.getSystemVersion(),

  //api config
  connection_timeout: 10000,
  network_checking_timeout: 10000,

  api_domain: 'http://lhp.n911.org/',
  ping_url: 'http://www.google.com.hk',

  showTNC: true,
}