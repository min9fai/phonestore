import {
  Alert,
} from 'react-native'

export const showAlert = (msg, title = 'Phone Store', onPress = () => { }) => {
  Alert.alert(
    title,
    msg,
    [
      { text: '確定', onPress: onPress },
    ],
    { cancelable: false }
  )
}