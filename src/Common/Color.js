
export default {
	main: 'skyblue',

	black: '#000',
	white: '#fff',
	transparent: 'transparent',

	divier: '#DBDBDB',
	badge: '#f10031',

	no_inventory: '#ff0000',
	has_inventory: '#01de52',
	has_discount: '#ff0000',
	btn_disable: '#e0e0e0',


}
