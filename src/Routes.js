import React from 'react'
import { createStackNavigator } from 'react-navigation'
import MainPage from './Container/MainPage'
import ProductDetailPage from './Container/ProductDetailPage'
import CartPage from './Container/CartPage'
import ProfilePage from './Container/ProfilePage'
import TNCPage from './Container/TNCPage'
import LoginPage from './Container/LoginPage'

import { size, color } from './Common'
import PaymentPage from './Container/PaymentPage'
import RegisterPage from './Container/RegisterPage'
import RecordPage from './Container/RecordPage'


export default createStackNavigator(
  {
    MainPage: MainPage,
    LoginPage: LoginPage,
    ProductDetailPage: ProductDetailPage,
    ProfilePage: ProfilePage,
    TNC: TNCPage,
    CartPage: CartPage,
    PaymentPage: PaymentPage,
    RegisterPage: RegisterPage,
    RecordPage: RecordPage,
  },
  {
    initialRouteName: 'MainPage',
    mode: 'modal',
    navigationOptions: {
      gesturesEnabled: false,
    },
    cardStyle: {
      paddingTop: size.statusBarHeight,
      paddingBottom: size.bottomHeight,
      backgroundColor: color.main,
    }
  }
)