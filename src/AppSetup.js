import React from 'react'
import { View, StyleSheet, Text } from 'react-native'

import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { PersistGate } from 'redux-persist/integration/react'
import { withNetworkConnectivity, createNetworkMiddleware } from 'react-native-offline'
import { baseReducer, userReducer, productReducer, network } from './Redux/Reducer'

import AppLauncher from './AppLauncher'
import { config } from './Common'

const AppContent = () => {
  return (
    <AppLauncher />
  )
}

const persistConfig = {
  version: 1,
  key: 'app',
  storage: storage,
  blacklist: ['userReducer', 'network'],
}

const pUserConfig = {
  version: 1,
  key: 'app_user',
  storage,
  blacklist: [
    'loginLoading',
    'logoutLoading',
    'registerLoading',
    'updateUserLoading',
    'fetchRecordLoading',
    'myReocrd'],
}

const pProductConfig = {
  version: 1,
  key: 'app_product',
  storage,
  blacklist: ['fetchDataLoading', 'addCartLoading', 'reduceCartLoading','paymentLoading'],
}

const rootReducer = combineReducers({
  baseReducer,
  productReducer: persistReducer(pProductConfig, productReducer),
  userReducer: persistReducer(pUserConfig, userReducer),
  network,
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

const AppWithNetwork = withNetworkConnectivity({
  pingServerUrl: config.ping_url,
  checkConnectionInterval: config.network_checking_timeout,
  withRedux: true,
})(AppContent)

const store = createStore(
  persistedReducer,
  applyMiddleware(thunk, createNetworkMiddleware())
)

const persistor = persistStore(store)

class AppSetup extends React.PureComponent {
  render() {
    console.log('render - App Setup')
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppWithNetwork />
        </PersistGate>
      </Provider>
    )
  }
}

export default AppSetup