import React from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'
import { connect } from 'react-redux'
import BackHeader from '../Component/Header/BackHeader'
import CusButton from '../Component/Button/CusButton'
import { color, size } from '../Common'
import { doLogoutAction, doUpdateUserAction } from '../Redux/Action/Action'
import IconButton from '../Component/Button/IconButton'
import TextEditModal from '../Component/Modal/TextEditModal'

class ProfilePage extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      showEditAddress: false,
    }

    this.logoutTap = this.logoutTap.bind(this)
    this.recordTap = this.recordTap.bind(this)
    this.openEditAddress = this.openEditAddress.bind(this)
    this.closeEditAddress = this.closeEditAddress.bind(this)
  }

  static navigationOptions = (props) => {
    return { header: <BackHeader {...props} title={'我的帳號'} /> }
  }

  componentDidMount() {
    console.log('Profile Page - componentDidMount')
  }

  logoutTap() {
    const { navigation } = this.props

    this.props.doLogoutAction().then(res => {
      if (res) {
        navigation.goBack()
      }
    })
  }

  recordTap() {
    const { navigation } = this.props
    navigation.navigate('RecordPage')
  }

  openEditAddress() {
    this.setState({
      showEditAddress: true,
    })
  }

  closeEditAddress(returnValue) {
    if (returnValue && returnValue !== '') {
      //update value
      this.props.doUpdateUserAction(returnValue).then(res=>{
        if ( !res.result && res.message ) {
          tools.showAlert(res.message)
        }
      })
    }

    this.setState({
      showEditAddress: false,
    })
  }

  render() {
    if (!this.props.userInfo)
      return null
    return (
      <View style={styles.container}>
        <View style={styles.wrapContainer}>
          <Image style={styles.profilePic} source={require('../Images/profilepic.png')} />
          <View style={styles.itemView}>
            <Text>{this.props.userInfo.last_name + this.props.userInfo.first_name}</Text>
          </View>
          <View style={styles.itemView}>
            <Text>{this.props.userInfo.account}</Text>
          </View>
          <View style={styles.itemView}>
            <Text style={{ flex: 1, textAlign: 'center' }}>{this.props.userInfo.email}</Text>
          </View>
          <View style={styles.itemView}>
            <View style={{ width: 20, height: 20 }} />
            <Text style={{ flex: 1, textAlign: 'center' }}>{this.props.userInfo.address}</Text>
            <IconButton iconName={'edit'} iconType={5} cusStyle={{ width: 20, height: 20 }} iconColor={color.main} onPress={this.openEditAddress} />
          </View>
          <CusButton title={'查詢我的訂單'} onPress={this.recordTap}
            cusContainerStyle={{ backgroundColor: color.white, width: '50%' }} />
          <View style={{ flex: 1 }}></View>
          <CusButton title={'登出'} onPress={this.logoutTap}
            cusContainerStyle={{ backgroundColor: color.main, width: '100%' }} />
          <TextEditModal isShowModal={this.state.showEditAddress}
            closeModal={this.closeEditAddress}
            title={'修改地址'} placeHolder={'輸入地址'}
            defValue={this.props.userInfo.address} />
        </View>
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  wrapContainer: {
    flex: 1,
    margin: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: color.white,
  },
  profilePic: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  itemView: {
    minHeight: 40,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  }
})

const mapStateToProps = state => {
  return {
    userInfo: state.userReducer.userInfo,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doLogoutAction: () => dispatch(doLogoutAction()),
    doUpdateUserAction: (address) => dispatch(doUpdateUserAction(address)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage)
