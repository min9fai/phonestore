import React from 'react'
import {
  View,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Button,
  Image,
} from 'react-native'
import { connect } from 'react-redux'

import { color, size, config, tools } from '../Common'
import BackHeader from '../Component/Header/BackHeader'
import { doLoginAction } from '../Redux/Action/Action'
import Indicator from '../Component/Indicator';

class LoginPage extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      account: '',
      password: '',
    }

    this.txtAccountRef = undefined
    this.txtPasswordRef = undefined

    this.loginTap = this.loginTap.bind(this)
    this.registerTap = this.registerTap.bind(this)
    this.navToTNC = this.navToTNC.bind(this)
  }

  static navigationOptions = (props) => {
    return { header: <BackHeader {...props} title={'登入'} /> }
  }

  componentDidMount() {
    console.log('Login Page - componentDidMount')
  }

  loginTap() {
    if (this.state.account.trim().length > 0 && this.state.password.trim().length > 0) {
      this.props.doLoginAction(this.state.account, this.state.password).then(
        res => {
          if (!res || !res.result) {
            tools.showAlert(res.message ? res.message : '登入失敗')
          } else {
            tools.showAlert('登入成功，歡迎 ' + res.user.account, 'phone Store', () => {
              this.props.navigation.goBack()
            })
          }
        }
      )
    } else if (this.state.account.trim().length <= 0) {
      tools.showAlert('請輸入帳號！', 'Phone Store', () => { this.txtAccountRef.focus() })
    } else if (this.state.password.trim().length <= 0) {
      tools.showAlert('請輸入秘密！', 'Phone Store', () => { this.txtPasswordRef.focus() })
    }
  }

  registerTap() {
    const { navigate } = this.props.navigation
    navigate('RegisterPage')
  }

  navToTNC() {
    const { navigate } = this.props.navigation
    navigate('TNC')
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
          <View style={styles.wrapContainer}>
            <Image
              style={styles.image}
              source={require('../Images/enter.png')}
            />
            <View style={styles.inputView}>
              <Text style={styles.text}>帳號 : </Text>
              <TextInput ref={ref => this.txtAccountRef = ref} autoCapitalize={'none'} autoCorrect={false}
                style={styles.input} placeholder={'帳號'} clearButtonMode={'while-editing'} returnKeyType={'next'}
                onSubmitEditing={() => { this.txtPasswordRef.focus() }}
                onChangeText={(text) => this.setState({ account: text })}
              />
            </View>
            <View style={styles.inputView}>
              <Text style={styles.text}>密碼 : </Text>
              <TextInput ref={ref => this.txtPasswordRef = ref} autoCapitalize={'none'} autoCorrect={false}
                style={styles.input} placeholder={'密碼'} secureTextEntry={true} clearButtonMode={'while-editing'}
                returnKeyType={'send'}
                onSubmitEditing={this.loginTap}
                onChangeText={(text) => this.setState({ password: text })}
              />
            </View>
            <Button title={'登入'} onPress={this.loginTap} />
            <Button title={'注冊'} onPress={this.registerTap} />
          </View>
        </TouchableWithoutFeedback>

        {
          config.showTNC &&
          <View style={styles.tncView}>
            <TouchableWithoutFeedback onPress={this.navToTNC}>
              <Text style={{ paddingLeft: 10, paddingRight: 10, }}>
                服務條款
            </Text>
            </TouchableWithoutFeedback>
          </View>
        }

        <Indicator loading={this.props.loginLoading} />
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  wrapContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 50,
    height: 50,
    margin: 20,
  },
  tncView: {
    bottom: 0,
    position: 'absolute',
    height: size.tncHeight,
    width: '100%',
    backgroundColor: color.main,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'flex-end'
  },
  inputView: {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    width: '100%',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
  text: {
    minWidth: 100,
    textAlign: 'right',
    paddingRight: 10,

  },
  input: {
    paddingRight: 10,
    paddingLeft: 10,
    height: 30,
    width: 200,
    borderColor: 'gray',
    borderWidth: 1,
  },
})

const mapStateToProps = state => {
  return {
    userInfo: state.userReducer.userInfo,
    loginLoading: state.userReducer.loginLoading,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doLoginAction: (account, password) => dispatch(doLoginAction(account, password))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)