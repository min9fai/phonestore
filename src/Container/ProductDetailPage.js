import React from 'react'
import { ScrollView, View, StyleSheet, Text, Image, Button } from 'react-native'
import { connect } from 'react-redux'
import { doAddCartAction } from '../Redux/Action/Action'
import MainHeader from '../Component/Header/MainHeader'
import { color, size, tools } from '../Common'
import CusButton from '../Component/Button/CusButton';
import ProductNameView from '../Component/Product/ProductNameView'
import ProductPriceView from '../Component/Product/ProductPriceView'

class ProductDetailPage extends React.PureComponent {
  constructor(props) {
    super(props)

    this.addToCartTap = this.addToCartTap.bind(this)
  }

  static navigationOptions = (props) => {
    return { header: <MainHeader {...props} hasBack={true} /> }
  }

  componentDidMount() {
    console.log('Setting Page - componentDidMount')
  }

  addToCartTap() {
    const { navigation } = this.props
    const selectedProduct = navigation.getParam('selectedProduct', undefined)
    if (!selectedProduct || selectedProduct.stock < 1)
      return

    this.props.doAddCartAction(selectedProduct).then(
      res => {
        if (res && res == 'OVER') {
          tools.showAlert('每樣物品不可購買多於2件!')
        }
      }
    )
  }

  render() {
    const { navigation } = this.props
    const selectedProduct = navigation.getParam('selectedProduct', undefined)
    if (!selectedProduct)
      navigation.goBack()

    return (
      <View style={styles.container}>
        <ScrollView style={{ flex: 1 }} bounces={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.contentView}>
            <Image
              style={styles.image}
              source={{ uri: selectedProduct.image }}
            />
            <View style={{ height: 20 }} />
            <View style={styles.itemView}>
              <Text style={styles.title}>品稱</Text>
              <View style={styles.divider} />
              <ProductNameView product={selectedProduct} />
            </View>
            <View style={styles.itemView}>
              <Text style={styles.title}>
                售價
              </Text>
              <View style={styles.divider} />
              <ProductPriceView product={selectedProduct} />
            </View>
            <View style={styles.itemView}>
              <Text style={styles.title}>
                詳情
              </Text>
              <View style={styles.divider} />
              <Text style={styles.value}>
                {selectedProduct.description}
              </Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.bottomView}>
          <CusButton title={'加入購物車'} onPress={this.addToCartTap}
            cusContainerStyle={{ backgroundColor: color.main, width: '80%' }}
            disabled={selectedProduct.stock <= 0} />
        </View>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: color.white,
  },
  contentView: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  image: {
    width: size.width * .6,
    height: size.width * .6,
  },
  itemView: {
    padding: 10,
    minHeight: 50,
    width: size.width - 50,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignContent: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: size.fs_big,
    marginBottom: 5,
  },
  value: {
    margin: 5,
    fontSize: size.fs_normal,
  },
  divider: {
    height: 1,
    width: '100%',
    backgroundColor: color.divier,
  },
  bottomView: {
    height: 60,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: color.white,
  },
})

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doAddCartAction: (product) => dispatch(doAddCartAction(product))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailPage)
