import React from 'react'
import {
  AppState,
  View,
  StyleSheet,
  Text,
  FlatList,
  TextInput,
  TouchableWithoutFeedback
} from 'react-native'
import { connect } from 'react-redux'
import { size, color, config } from '../Common'
import { doFetchDataAction } from '../Redux/Action/Action'
import MainHeader from '../Component/Header/MainHeader'
import ProductCell from '../Component/Cell/ProductCell'
import * as data from '../data'
import IconButton from '../Component/Button/IconButton';
import CusButton from '../Component/Button/CusButton';

class RecordPage extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      searchText: '',
      products: props.productList,
    }

    this.navToTNC = this.navToTNC.bind(this)
    this.refreshTap = this.refreshTap.bind(this)
    this.filterAction = this.filterAction.bind(this)
    this.resetAction = this.resetAction.bind(this)

    this.getItemLayout = this.getItemLayout.bind(this)
    this.productCellRender = this.productCellRender.bind(this)
    this.searchViewRender = this.searchViewRender.bind(this)
    this.emptyDataRender = this.emptyDataRender.bind(this)

  }

  static navigationOptions = (props) => {
    return { header: <MainHeader {...props} showIcon={true} /> }
  }

  componentWillMount() {
    this.refreshTap()
  }

  componentDidMount() {
    console.log('Product Page - componentDidMount')
  }

  componentWillUnmount() {
    console.log('Product Page - componentWillUnmount')
  }

  navToTNC() {
    const { navigate } = this.props.navigation
    navigate('TNC')
  }

  refreshTap() {
    this.props.doFetchDataAction().then(res => {
      if (res && res.result && res.products) {
        this.setState({
          products: res.products,
        })
      }
    })
  }

  filterAction() {
    if (this.state.searchText.trim() === '')
      return
    const filteredList = this.state.products.filter(pro => {
      return pro.name.includes(this.state.searchText)
    })

    this.setState({
      products: filteredList,
    })
  }

  resetAction() {
    this.setState({
      products: this.props.productList,
      searchText: '',
    })
  }
  getItemLayout(data, index) {
    return { length: size.width / 2, offset: size.width / 2 * index, index }
  }

  productCellRender(props) {
    return <ProductCell {...props} {...this.props} />
  }

  searchViewRender() {
    return (
      <View style={{
        height: 50,
        width: size.width,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'flex-start',
        padding: 10,
        backgroundColor: color.main,
      }}>
        <TextInput
          style={{ height: 30, flex: 1, backgroundColor: color.white, borderRadius: 5, paddingLeft: 5, paddingRight: 5, }}
          value={this.state.searchText}
          autoCapitalize={'none'}
          autoCorrect={false}
          placeholder={'搜索產品名字'}
          clearButtonMode={'while-editing'}
          returnKeyType={'search'}
          onChangeText={(text) => { this.setState({ searchText: text }) }}
          onSubmitEditing={this.filterAction}
        />
        <CusButton title={'還原'} onPress={this.resetAction}
          cusContainerStyle={{ backgroundColor: color.divier, minWidth: 40, height: 30, borderRadius: 5, marginRight: 0, }} />
      </View>
    )
  }

  emptyDataRender() {
    return (
      <View style={{
        padding: 15,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
      }}>
        <Text>沒有任何產品！</Text>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={{ flex: 1 }}
          keyExtractor={item => item.id}
          data={this.state.products}
          renderItem={this.productCellRender}
          ListHeaderComponent={this.searchViewRender}
          ListEmptyComponent={this.emptyDataRender}
          getItemLayout={this.getItemLayout}
          numColumns={2}
        />
        {
          config.showTNC &&
          <View style={styles.tncView}>
            <TouchableWithoutFeedback onPress={this.navToTNC}>
              <Text style={{ paddingLeft: 10, paddingRight: 10, }}>
                服務條款
            </Text>
            </TouchableWithoutFeedback>
          </View>
        }
        <IconButton
          cusStyle={styles.refreshButtonView}
          iconName='undo-alt'
          onPress={this.refreshTap}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  tncView: {
    height: size.tncHeight,
    width: '100%',
    backgroundColor: color.main,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'flex-end'
  },
  refreshButtonView: {
    backgroundColor: color.main,
    height: 40,
    width: 40,
    borderRadius: 20,
    position: 'absolute',
    bottom: 20,
    left: 20,
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: 1.0,
  }
})

const mapStateToProps = state => {
  return {
    productList: state.productReducer.productList,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doFetchDataAction: () => dispatch(doFetchDataAction())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecordPage)
