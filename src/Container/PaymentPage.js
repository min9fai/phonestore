import React from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'
import { connect } from 'react-redux'
import { StackActions, NavigationActions } from 'react-navigation'

import BackHeader from '../Component/Header/BackHeader'
import CusButton from '../Component/Button/CusButton'
import { color, size, tools } from '../Common';

import BraintreeDropIn from 'react-native-braintree-dropin-ui'
import { doPaymentAction } from '../Redux/Action/Action';


class PaymentPage extends React.PureComponent {
  constructor(props) {
    super(props)

    this.paymenyTap = this.paymenyTap.bind(this)
  }

  static navigationOptions = (props) => {
    return { header: <BackHeader {...props} title={'結賬'} /> }
  }

  componentDidMount() {
    console.log('Profile Page - componentDidMount')
  }

  paymenyTap() {
    let totalPrice = 0
    this.props.cartList.forEach(item => {
      totalPrice += item.price * item.discount * item.amount
    })

    const { navigation } = this.props
    const ordernumber = navigation.getParam('ordernumber', undefined)

    //sandbox_xycj4n43_gn2h7f3wvfqhtcjs
    //sandbox_wj2d9bjf_7mzt9s9f5jqbv9hg
    const sandboxToken = 'sandbox_xycj4n43_gn2h7f3wvfqhtcjs'

    BraintreeDropIn.show({
      clientToken: sandboxToken,
      merchantIdentifier: 'applePayMerchantIdentifier',
      countryCode: 'HK',
      currencyCode: 'HKD',
      merchantName: 'Your Merchant Name for Apple Pay',
      orderTotal: totalPrice,
      googlePay: false,
      applePay: false,
    }).then(result => {
      if (result) {
        const { type, nonce } = result
        if (type && nonce) {
          this.props.doPaymentAction(nonce, ordernumber, type).then(
            res => {
              if (res && res.result) {
                tools.showAlert('付款成功!', 'Phone Store', () => {
                  this.props.navigation.dispatch(
                    StackActions.reset({
                      index: 0,
                      actions: [
                        NavigationActions.navigate({ routeName: 'MainPage' }),
                      ],
                    }),
                  )
                })
              } else if (res.message) {
                tools.showAlert('付款失敗\r\n' + res.message)
              } else {
                tools.showAlert('付款失敗，請稍後再試！')
              }
            }
          )
        }
      }
    }).catch(error => {
      console.log('doPaymentAction', error)

      if (error.code === 'USER_CANCELLATION') {
        // update your UI to handle cancellation
      } else {
        tools.showAlert('付款失敗，請稍後再些！')
      }
    })
  }

  render() {
    let totalAmount = 0
    let totalPrice = 0
    this.props.cartList.forEach(item => {
      totalAmount += item.amount
      totalPrice += item.price * item.discount * item.amount
    })

    const { navigation } = this.props
    const ordernumber = navigation.getParam('ordernumber', undefined)

    return (
      <View style={styles.container}>
        <View style={styles.wrapContainer}>
          <View style={styles.itemView}>
            <Text style={styles.titleText}>{`單號`}</Text>
            <Text style={styles.contentText}>{ordernumber}</Text>
          </View>
          <View style={styles.itemView}>
            <Text style={styles.titleText}>{`總數 ( ${totalAmount}件 )`}</Text>
            <Text style={styles.contentText}>{`$${totalPrice}`}</Text>
          </View>
          <View style={styles.itemView}>
            <Text style={styles.titleText}>{`電郵`}</Text>
            <Text style={styles.contentText}>{this.props.userInfo.email}</Text>
          </View>
          <View style={styles.itemView}>
            <Text style={styles.titleText}>{`送貨地址`}</Text>
            <Text style={styles.contentText}>{this.props.userInfo.address}</Text>
          </View>
          <View style={{ flex: 1 }}></View>
          <CusButton title={'結帳'} onPress={this.paymenyTap}
            cusContainerStyle={{ backgroundColor: color.main, width: '100%' }} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  wrapContainer: {
    flex: 1,
    margin: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: color.white,
  },
  itemView: {
    minHeight: 40,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'flex-start',
  },
  titleText: {
    minWidth: 100,
  },
  contentText: {
    flex: 1,
    textAlign: 'right',
  }
})

const mapStateToProps = state => {
  return {
    userInfo: state.userReducer.userInfo,

    cartList: state.productReducer.cartList,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doPaymentAction: (nonce, ordernumber, paymentMethod) => dispatch(doPaymentAction(nonce, ordernumber, paymentMethod))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentPage)
