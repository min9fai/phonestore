import React from 'react'
import { View, StyleSheet, Text, FlatList } from 'react-native'
import { connect } from 'react-redux'
import BackHeader from '../Component/Header/BackHeader'
import CartItemCell from '../Component/Cell/CartItemCell'
import { color } from '../Common'
import CusButton from '../Component/Button/CusButton'

class CartPage extends React.PureComponent {
  constructor(props) {
    super(props)

    this.cartCellRender = this.cartCellRender.bind(this)
    this.emptyCartRender = this.emptyCartRender.bind(this)
    this.cartDetailRender = this.cartDetailRender.bind(this)

    this.paymentTap = this.paymentTap.bind(this)
  }

  static navigationOptions = (props) => {
    return { header: <BackHeader {...props} showLogin={true} title={'我的購物車'} /> }
  }

  componentDidMount() {
    console.log('Cart Page - componentDidMount')
  }

  cartCellRender(props) {
    return <CartItemCell {...props} {...this.props} />
  }

  emptyCartRender() {
    return (
      <View style={{
        padding: 15,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
      }}>
        <Text>你的購物車沒有商品！</Text>
      </View>
    )
  }

  cartDetailRender() {
    let totalAmount = 0
    let totalPrice = 0
    this.props.cartList.forEach(item => {
      totalAmount += item.amount
      totalPrice += item.price * item.discount * item.amount
    })

    if (totalAmount <= 0)
      return null

    return (
      <View style={styles.cartDetailView}>
        <View style={{ flexDirection: 'row', marginBottom: 5, }}>
          <Text>{`總數 ( ${totalAmount}件 )`}</Text>
          <View style={{ flex: 1 }} />
          <Text>{`$${totalPrice}`}</Text>
        </View>
        <View style={styles.divier} />
        <CusButton title={'立即結算'}
          cusContainerStyle={{ backgroundColor: color.main, width: '100%', marginTop: 5, }}
          onPress={this.paymentTap} />
      </View>
    )
  }

  paymentTap() {
    const { navigation } = this.props

    if (!this.props.userInfo) {
      //go login first
      navigation.navigate('LoginPage')
    } else {
      //go set address and payment
      const ordernumber = 'PS_' + Math.floor(Math.random() * Math.floor(1000)) +
        Date.parse(new Date().toISOString())
      navigation.navigate('PaymentPage', { ordernumber: ordernumber })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={{ flex: 1 }}
          keyExtractor={item => item.id}
          data={this.props.cartList}
          renderItem={this.cartCellRender}
          ListEmptyComponent={this.emptyCartRender}
        />
        {this.cartDetailRender()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: color.white,
  },
  cartDetailView: {
    padding: 10,
    width: '100%',
    backgroundColor: color.divier,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  divier: {
    width: '100%',
    height: 1,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: color.black,
  }
})

const mapStateToProps = state => {
  return {
    userInfo: state.userReducer.userInfo,

    cartList: state.productReducer.cartList,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartPage)