import React from 'react'
import {
  View,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Button,
  Image,
} from 'react-native'
import { connect } from 'react-redux'

import { color, size, config, tools } from '../Common'
import BackHeader from '../Component/Header/BackHeader'
import { doRegisterAction } from '../Redux/Action/Action'
import Indicator from '../Component/Indicator';

class RegisterPage extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      lastName: '',
      firstName: '',
      account: '',
      password: '',
      email: '',
      address: '',
    }

    this.txtLastNameRef = undefined
    this.txtFirstNameRef = undefined
    this.txtAccountRef = undefined
    this.txtPasswordRef = undefined
    this.txtEmailRef = undefined
    this.txtAddressRef = undefined

    this.registerTap = this.registerTap.bind(this)
    this.navToTNC = this.navToTNC.bind(this)
  }

  static navigationOptions = (props) => {
    return { header: <BackHeader {...props} title={'注冊'} /> }
  }

  componentDidMount() {
    console.log('Login Page - componentDidMount')
  }

  registerTap() {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

    if (this.state.lastName.trim().length <= 0) {
      tools.showAlert('請輸入姓氏！', 'Phone Store', () => { this.txtLastNameRef.focus() })
    } else if (this.state.firstName.trim().length <= 0) {
      tools.showAlert('請輸入姓名！', 'Phone Store', () => { this.txtFirstNameRef.focus() })
    } else if (this.state.account.trim().length <= 0) {
      tools.showAlert('請輸入帳號！', 'Phone Store', () => { this.txtAccountRef.focus() })
    } else if (this.state.account.trim().length < 4) {
      tools.showAlert('帳號必須4字或以上！！', 'Phone Store', () => { this.txtAccountRef.focus() })
    } else if (this.state.password.trim().length <= 0) {
      tools.showAlert('請輸入密碼！', 'Phone Store', () => { this.txtPasswordRef.focus() })
    } else if (this.state.password.trim().length < 6) {
      tools.showAlert('密碼必須6字或以上！', 'Phone Store', () => { this.txtPasswordRef.focus() })
    } else if (this.state.email.trim().length <= 0) {
      tools.showAlert('請輸入電郵！', 'Phone Store', () => { this.txtEmailRef.focus() })
    } else if (!reg.test(this.state.email)) {
      tools.showAlert('電郵格式不正確！', 'Phone Store', () => { this.txtEmailRef.focus() })
    } else if (this.state.address.trim().length <= 0) {
      tools.showAlert('請輸入送貨地址！', 'Phone Store', () => { this.txtAddressRef.focus() })
    } else {
      this.props.doRegisterAction(
        this.state.lastName,
        this.state.firstName,
        this.state.account,
        this.state.password,
        this.state.email,
        this.state.address
      ).then(
        res => {
          if (!res) {
            tools.showAlert('注冊失敗')
          } else if (!res.result) {
            tools.showAlert('注冊失敗\r\n' + res.message)
          } else {
            tools.showAlert('注冊成功', 'phone Store', () => {
              this.props.navigation.goBack()
            })
          }
        }
      )
    }
  }

  navToTNC() {
    const { navigate } = this.props.navigation
    navigate('TNC')
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
          <View style={styles.wrapContainer}>
            <Image
              style={styles.image}
              source={require('../Images/clipboard.png')}
            />
            <View style={styles.inputView}>
              <Text style={styles.text}>姓氏* : </Text>
              <TextInput ref={ref => this.txtLastNameRef = ref} autoCapitalize={'none'} autoCorrect={false}
                style={styles.input} placeholder={'姓氏'} clearButtonMode={'while-editing'} returnKeyType={'next'}
                onSubmitEditing={() => { this.txtFirstNameRef.focus() }}
                onChangeText={(text) => this.setState({ lastName: text })}
              />
            </View>
            <View style={styles.inputView}>
              <Text style={styles.text}>姓名* : </Text>
              <TextInput ref={ref => this.txtFirstNameRef = ref} autoCapitalize={'none'} autoCorrect={false}
                style={styles.input} placeholder={'姓名'} clearButtonMode={'while-editing'}
                returnKeyType={'next'}
                onSubmitEditing={() => { this.txtAccountRef.focus() }}
                onChangeText={(text) => this.setState({ firstName: text })}
              />
            </View>
            <View style={styles.inputView}>
              <Text style={styles.text}>帳號* : </Text>
              <TextInput ref={ref => this.txtAccountRef = ref} autoCapitalize={'none'} autoCorrect={false}
                style={styles.input} placeholder={'帳號(4字或以上)'} clearButtonMode={'while-editing'}
                returnKeyType={'next'}
                onSubmitEditing={() => { this.txtPasswordRef.focus() }}
                onChangeText={(text) => this.setState({ account: text })}
              />
            </View>
            <View style={styles.inputView}>
              <Text style={styles.text}>密碼 : </Text>
              <TextInput ref={ref => this.txtPasswordRef = ref} autoCapitalize={'none'} autoCorrect={false}
                style={styles.input} placeholder={'密碼(6字或以上)'} secureTextEntry={true} clearButtonMode={'while-editing'}
                returnKeyType={'next'}
                onSubmitEditing={() => { this.txtEmailRef.focus() }}
                onChangeText={(text) => this.setState({ password: text })}
              />
            </View>
            <View style={styles.inputView}>
              <Text style={styles.text}>電郵* : </Text>
              <TextInput ref={ref => this.txtEmailRef = ref} autoCapitalize={'none'} autoCorrect={false}
                style={styles.input} placeholder={'電郵'} clearButtonMode={'while-editing'}
                returnKeyType={'next'}
                onSubmitEditing={() => { this.txtAddressRef.focus() }}
                onChangeText={(text) => this.setState({ email: text })}
              />
            </View>
            <View style={styles.inputView}>
              <Text style={styles.text}>送貨地址* : </Text>
              <TextInput ref={ref => this.txtAddressRef = ref} autoCapitalize={'none'} autoCorrect={false}
                style={styles.input} placeholder={'送貨地址'} clearButtonMode={'while-editing'}
                returnKeyType={'done'}
                onSubmitEditing={this.registerTap}
                onChangeText={(text) => this.setState({ address: text })}
              />
            </View>
            <View style={[styles.inputView]}>
              <Text style={[styles.text, {
                fontSize: size.fs_normal,
                textAlign: 'center',
                flex: 1,
                color: '#f00',
              }]}>
                {'* 全部資料必需填寫'}
              </Text>
            </View>
            <View style={[styles.inputView]}>
              <Text style={[styles.text, {
                fontSize: size.fs_normal,
                textAlign: 'center',
                flex: 1,
                color: '#f00',
              }]}>
                {'注冊代表同意應用之服務條款'}
              </Text>
            </View>
            <Button title={'注冊'} onPress={this.registerTap} />

          </View>
        </TouchableWithoutFeedback>

        {
          config.showTNC &&
          <View style={styles.tncView}>
            <TouchableWithoutFeedback onPress={this.navToTNC}>
              <Text style={{ paddingLeft: 10, paddingRight: 10, }}>
                服務條款
            </Text>
            </TouchableWithoutFeedback>
          </View>
        }

        <Indicator loading={this.props.loginLoading} />
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  wrapContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 50,
    height: 50,
    margin: 20,
  },
  tncView: {
    bottom: 0,
    position: 'absolute',
    height: size.tncHeight,
    width: '100%',
    backgroundColor: color.main,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'flex-end'
  },
  inputView: {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    width: '100%',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
  text: {
    minWidth: 100,
    textAlign: 'right',
    paddingRight: 10,

  },
  input: {
    paddingRight: 10,
    paddingLeft: 10,
    height: 30,
    width: 200,
    borderColor: 'gray',
    borderWidth: 1,
  },
})

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doRegisterAction: (last_name, first_name, account, password, email, address) => dispatch(doRegisterAction(last_name, first_name, account, password, email, address))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage)