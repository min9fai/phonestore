import React from 'react'
import { View, StyleSheet, Text, FlatList } from 'react-native'
import { connect } from 'react-redux'
import BackHeader from '../Component/Header/BackHeader'
import CartItemCell from '../Component/Cell/CartItemCell'
import { color } from '../Common'
import CusButton from '../Component/Button/CusButton'
import { doFetchRecordAction } from '../Redux/Action/Action';
import RecordCell from '../Component/Cell/RecordCell';

class RecordPage extends React.PureComponent {
  constructor(props) {
    super(props)

    this.cartCellRender = this.cartCellRender.bind(this)
    this.emptyRecordRender = this.emptyRecordRender.bind(this)
    this.cartDetailRender = this.cartDetailRender.bind(this)
  }

  static navigationOptions = (props) => {
    return { header: <BackHeader {...props} showLogin={true} title={'我的購物車'} /> }
  }

  componentDidMount() {
    console.log('Record Page - componentDidMount')
    this.props.doFetchRecordAction()
  }

  cartCellRender(props) {
    return <RecordCell {...props} {...this.props} />
  }

  emptyRecordRender() {
    return (
      <View style={{
        padding: 15,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
      }}>
        <Text>你還沒有任何訂單！</Text>
      </View>
    )
  }

  cartDetailRender() {
    return null
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={{ flex: 1 }}
          keyExtractor={item => item.ordernumber}
          data={this.props.myReocrd}
          renderItem={this.cartCellRender}
          ListEmptyComponent={this.emptyRecordRender}
        />
        {this.cartDetailRender()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: color.white,
  },
  cartDetailView: {
    padding: 10,
    width: '100%',
    backgroundColor: color.divier,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  divier: {
    width: '100%',
    height: 1,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: color.black,
  }
})

const mapStateToProps = state => {
  return {
    userInfo: state.userReducer.userInfo,
    myReocrd: state.userReducer.myReocrd,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doFetchRecordAction: () => dispatch(doFetchRecordAction())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecordPage)