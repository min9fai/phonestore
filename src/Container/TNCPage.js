import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import BackHeader from '../Component/Header/BackHeader'
import { color, size } from '../Common'

class TNCPage extends React.PureComponent {
  constructor(props) {
    super(props)
  }

  static navigationOptions = (props) => {
    return { header: <BackHeader {...props} title={'服務條款'} /> }
  }

  componentDidMount() {
    console.log('TNCPage Page - componentDidMount')
  }

  render() {
    return (
      <View style={styles.container}>
        <Text text={styles.text}>
          {'條款1, ....'}
        </Text>
        <Text text={styles.text}>
          {'條款2, ....'}
        </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
    padding: 10,
  },
  text: {
    fontSize: size.fs_big,
  }
})

export default TNCPage