/** @format */

import { AppRegistry } from 'react-native'
import AppSetup from './src/AppSetup'
import { name as appName } from './app.json'

AppRegistry.registerComponent(appName, () => AppSetup)
